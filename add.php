<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'connectDB.php';


if(isset($_POST['titrefilm']) && !empty($_POST['titrefilm'])
  && isset($_POST['photokeanu']) && !empty($_POST['photokeanu'])
  && isset($_POST['characterkeanu']) && !empty($_POST['characterkeanu'])){
    $tfilm= htmlspecialchars($_POST['titrefilm']);
    $pkeanu= htmlspecialchars($_POST['photokeanu']);
    $charkeanu= htmlspecialchars($_POST['characterkeanu']);

            $reqSQL= "insert into film (titre, imgurl, personnage) values (:titre, :imgurl, :personnage)";
            $requete = $pdo->prepare($reqSQL);

            $requete -> bindValue(':titre', $tfilm, PDO::PARAM_STR);
            $requete -> bindValue(':imgurl', $pkeanu, PDO::PARAM_STR);
            $requete -> bindValue(':personnage', $charkeanu, PDO::PARAM_STR);

            $requete->execute();
            //$_SESSION['message'] = "Produit ajouté avec succès !";
            header('Location: index.php');
        }else{
          header('Location: add-form.php');
        }

?>
