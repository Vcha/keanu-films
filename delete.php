<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'connectDB.php';

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = strip_tags($_GET['id']);
    $reqSQL = "DELETE FROM film WHERE id=:id;";

    $requete = $pdo->prepare($reqSQL);

    $requete->bindValue(':id', $id, PDO::PARAM_INT);
    $requete->execute();

    header('Location: index.php');
}

 ?>
