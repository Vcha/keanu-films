<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Keanu</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
		<button type="button" class="btn btn-dark m-5"><a class="text-white text-decoration-none" href="add-form.php"> Add a new film</a></button>

		<div class="row mb-5">
			<?php
			include 'connectDB.php';

			$reqSQL= "select * from film";
			$requete = $pdo->prepare($reqSQL);
			$requete->execute();
			$tab = $requete->fetchAll();

			$i=0;
			while ($i<count($tab)){
			  $id = $tab[$i]['id'];
			  $photo = $tab[$i]['imgurl'];
			  $titre = $tab[$i]['titre'];
			  $perso = $tab[$i]['personnage'];
			  include 'card.php';
			  $i++;
			}
			?>
		</div>

	</div>

</body>
</html>
