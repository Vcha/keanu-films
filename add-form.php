<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add a new Film</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>

	<div class="container align-self-center mt-5">
		<h1 class="text-center">Add a new film of Keanu</h1>

		<form action="add.php" method="post">
			<div class="form-group border border-dark p-3">
				<label>Title:</label>
				<input name="titrefilm" type="text" class="form-control" placeholder="ex: Matrix" required>

			<div class="form-group">
				<label>URL image:</label>
				<input name="photokeanu" type="text" class="form-control" placeholder="img/keanu.jpg" required>
			</div>
			<div class="form-group">
				<label>Character:</label>
				<input name="characterkeanu" type="text" class="form-control" placeholder="ex: Neo" required>
			</div>
			<input type="submit" class="btn btn-primary">
		</form>
	</div>

	<div class="retour">
		<a class="btn btn-secondary" href="index.php">Retour à la liste</a>
	</div>
</body>
</html>
