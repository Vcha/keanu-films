
	<div class="col-sm-12 col-md-6 col-xl-4 mt-3">
		<div class="card">
			<img class="card-img-top" alt="photo" src= <?php echo $photo; ?> >
			<div class="card-body">

				<h5 class="card-title"><?php echo $titre; ?></h5>
				<p class="card-text"><?php echo $perso; ?></p>
				<a class="btn btn-secondary" href="delete.php?id=<?= $id ?>">Supprimer</a>

			</div>
		</div>
	</div>
